# Welcome!

I assume that you are looking for some information about making AI using State Machine in Unity.
So I would like to say that you are in the right place!
I've made the post on my blog describing this design pattern and making of simple AI. You can find my post here: https://www.patrykgalach.com/2019/03/21/using-state-machine-for-ai-in-unity/

Enjoy!

---

# How to use it?

This repository contains an example of how you can implement simple AI using State Machine in Unity.

If you want to see that implementation of it, go straight to [Assets/Example/Scripts/](https://bitbucket.org/gaello/statemachinewithai/src/master/Assets/Example/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.
I hope you will enjoy it!

---

#Well done!

You have just learned how to implement State Machine in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com


#Assets used in project

Castle Kit from [Kenney.nl](http://kenney.nl)
Standard Assets Example Project from [Unity](https://unity.com)
