﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vehicle : Character
{
    public override EntityTypeEnum Type => EntityTypeEnum.Vehicle;
}
