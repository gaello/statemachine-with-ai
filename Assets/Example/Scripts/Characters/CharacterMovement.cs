﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterMovement : MonoBehaviour
{
    [HideInInspector]
    public Character Owner;

    [SerializeField]
    private float speed = 5f;
    public float Speed => speed;

    [SerializeField]
    private float rotateSpeed = 30f;
    public float RotateSpeed => rotateSpeed;

    [HideInInspector]
    public float ReachDistance;

    [SerializeField]
    private CharacterController movement;

    private bool isMoving = false;
    public bool IsMoving => isMoving;

    private Vector3 targetPosition;

    public UnityAction OnPositionReached;

    public void MoveToPosition(Vector3 target)
    {
        targetPosition = target;
        isMoving = true;
    }

    public void StopMoving()
    {
        isMoving = false;
    }

    private void TargetReached()
    {
        OnPositionReached?.Invoke();
    }

    public void RotateTowardsPoint(Vector3 point)
    {
        var dir = point - transform.position;
        dir.Normalize();

        transform.Rotate(Vector3.up, Vector3.SignedAngle(-transform.right, dir, Vector3.up) * rotateSpeed * Time.deltaTime);
    }

    private void Update()
    {
        if (!isMoving)
        {
            return;
        }

        // movement
        var dir = targetPosition - transform.position;
        dir.Normalize();

        transform.Rotate(Vector3.up, Vector3.SignedAngle(-transform.right, dir, Vector3.up) * rotateSpeed * Time.deltaTime);

        var dot = Vector3.Dot(dir, -transform.right);
        movement.SimpleMove(Mathf.Max(dot, 0) * dir * speed * Time.deltaTime);

        if (Vector3.Distance(transform.position, targetPosition) < ReachDistance)
        {
            TargetReached();
        }
    }
}
