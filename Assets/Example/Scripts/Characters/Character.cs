﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Entity
{
    public override EntityTypeEnum Type => EntityTypeEnum.Character;

    [Header("References")]
    [SerializeField]
    private CharacterAttact attact;
    public CharacterAttact Attact => attact;

    [SerializeField]
    private CharacterMovement movement;
    public CharacterMovement Movement => movement;

    [Space]
    [SerializeField]
    private StateMachine ai;

    private void Awake()
    {
        attact.Owner = this;
        (Health as CharacterHealth).Owner = this;
        movement.Owner = this;
        ai.Character = this;

        Health.OnEntityKilled += Health_OnEntityKilled;
    }

    private void Start()
    {
        ai.ChangeState(new WaitState());
    }

    private void Health_OnEntityKilled()
    {
        ai.ChangeState(new DiedState());
    }

}
