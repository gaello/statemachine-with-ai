﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class CharacterAttact : MonoBehaviour
{
    [HideInInspector]
    public Character Owner;

    [SerializeField]
    private int attackDamage = 20;
    public int AttackDamage => attackDamage;

    [SerializeField]
    private float attackRange = 0.2f;
    public float AttackRange => attackRange;

    [SerializeField]
    private float attackCooldown = 0.5f;
    private float lastAttackTime;

    [Space]
    [SerializeField]
    private float maxDetectionRange = 20;

    [Space]
    [SerializeField]
    private List<EntityTypeEnum> targetList;
    public List<EntityTypeEnum> TargetList => targetList;

    [HideInInspector]
    public Entity targetEntity;


    private bool isAttacking = false;
    public bool IsAttacking => isAttacking;

    public UnityAction OnTargetKilled;
    public UnityAction OnTargetToFar;

    public Entity FindNewTarget()
    {
        var targets = FindObjectsOfType<Entity>().Where(e => e != Owner && e.Owner != Owner.Owner && targetList.Contains(e.Type) && Vector3.Distance(transform.position, e.transform.position) < maxDetectionRange).ToList();

        if (targets.Count > 0)
        {
            var closestTarget = targets[0];
            for (int i = 1; i < targets.Count; i++)
            {
                if (Vector3.Distance(transform.position, closestTarget.transform.position) > Vector3.Distance(transform.position, targets[i].transform.position))
                {
                    closestTarget = targets[i];
                }
            }
            return closestTarget;
        }

        return null;
    }

    public bool CanAttackTarget(Entity target)
    {
        if (!target)
            return false;

        // need target center or edge position
        return Vector3.Distance(transform.position, target.transform.position) < attackRange;
    }

    public void Attack(Entity target)
    {
        targetEntity = target;
        isAttacking = true;
    }

    private void Update()
    {
        if (!isAttacking)
        {
            return;
        }

        // check if can reach target to do damage
        // event if can't
        if (!CanAttackTarget(targetEntity))
        {
            OnTargetToFar?.Invoke();
            isAttacking = false;
            return;
        }

        Owner.Movement.RotateTowardsPoint(targetEntity.transform.position);

        if (Time.time - lastAttackTime >= attackCooldown)
        {
            lastAttackTime = Time.time;

            // do damage
            targetEntity.Health.TakeDamage(attackDamage);

            // check if killed
            // event if can't
            if (!targetEntity.Health.IsAlive)
            {
                OnTargetKilled?.Invoke();
                isAttacking = false;
            }
        }
    }
}
