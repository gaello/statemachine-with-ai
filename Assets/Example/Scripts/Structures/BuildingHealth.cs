﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingHealth : EntityHealth
{
    [HideInInspector]
    public Building Owner;
}
