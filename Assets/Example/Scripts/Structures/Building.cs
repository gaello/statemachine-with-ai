﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : Entity
{
    public override EntityTypeEnum Type => EntityTypeEnum.Structure;

    private void Awake()
    {
        (Health as BuildingHealth).Owner = this;

        Health.OnEntityKilled += Health_OnEntityKilled;
    }

    private void Health_OnEntityKilled()
    {
        Health.KillEntity();
    }
}
