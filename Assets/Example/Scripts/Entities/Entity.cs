﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour
{
    [Header("Player Setup")]
    [SerializeField]
    private OwnerEnum owner;
    public OwnerEnum Owner => owner;

    public virtual EntityTypeEnum Type => EntityTypeEnum.Character;

    [Header("References")]
    [SerializeField]
    private EntityHealth health;
    public EntityHealth Health => health;
}
