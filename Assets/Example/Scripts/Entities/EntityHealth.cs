﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EntityHealth : MonoBehaviour
{
    [SerializeField]
    private int health = 100;
    public bool IsAlive => health > 0;

    public UnityAction OnEntityKilled;

    public void TakeDamage(int dmg)
    {
        health -= dmg;

        if (health <= 0)
        {
            health = 0;
            OnEntityKilled?.Invoke();
        }
    }

    public void KillEntity()
    {
        Destroy(gameObject);
    }
}
