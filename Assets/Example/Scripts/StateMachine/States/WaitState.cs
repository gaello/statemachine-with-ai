﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitState : BaseState
{
    public float minWaitTime = 1;
    public float maxWaitTime = 2.5f;

    private float timeout;
    private float startTime;

    public override void PrepareState()
    {
        base.PrepareState();

        timeout = Random.Range(minWaitTime, maxWaitTime);
        startTime = Time.time;
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (Time.time - startTime >= timeout)
        {
            owner.ChangeState(new FindTargetState());
        }
    }
}
