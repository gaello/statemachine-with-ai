﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindTargetState : BaseState
{
    public override void PrepareState()
    {
        base.PrepareState();

        var newTarget = owner.Character.Attact.FindNewTarget();
        var move = new MoveState();

        if (newTarget)
        {
            move.entity = newTarget;
        }
        else
        {
            move.position = owner.transform.position + new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
        }

        owner.ChangeState(move);
    }
}
