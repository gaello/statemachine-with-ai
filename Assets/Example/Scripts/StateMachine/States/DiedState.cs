﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiedState : BaseState
{
    public override void PrepareState()
    {
        base.PrepareState();

        owner.ChangeState(null);
    }

    public override void DestroyState()
    {
        // kill entity!
        owner.Character.Health.KillEntity();

        base.DestroyState();
    }
}
