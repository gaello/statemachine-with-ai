﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : BaseState
{
    public Entity targetEntity;

    public override void PrepareState()
    {
        base.PrepareState();

        owner.Character.Attact.OnTargetKilled += Attact_OnTargetKilled;
        owner.Character.Attact.OnTargetToFar += Attact_OnTargetToFar;

        owner.Character.Attact.Attack(targetEntity);
    }

    public override void DestroyState()
    {
        owner.Character.Attact.OnTargetKilled -= Attact_OnTargetKilled;
        owner.Character.Attact.OnTargetToFar -= Attact_OnTargetToFar;

        base.DestroyState();
    }

    private void Attact_OnTargetKilled()
    {
        owner.ChangeState(new WaitState());
    }

    private void Attact_OnTargetToFar()
    {
        var move = new MoveState();
        move.entity = targetEntity;
        owner.ChangeState(move);
    }

}
