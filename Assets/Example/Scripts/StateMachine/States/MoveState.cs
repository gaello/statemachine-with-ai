﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveState : BaseState
{
    public Vector3 position;
    public Entity entity;

    public override void PrepareState()
    {
        base.PrepareState();

        owner.Character.Movement.ReachDistance = owner.Character.Attact.AttackRange * 0.9f;
        owner.Character.Movement.OnPositionReached += Movement_OnPositionReached;

        if (!entity)
        {
            owner.Character.Movement.MoveToPosition(position);
        }
    }

    public override void UpdateState()
    {
        base.UpdateState();

        if (!entity)
        {
            return;
        }

        // check if is alive ?

        owner.Character.Movement.MoveToPosition(entity.transform.position);
    }

    public override void DestroyState()
    {
        owner.Character.Movement.OnPositionReached -= Movement_OnPositionReached;

        base.DestroyState();
    }

    private void Movement_OnPositionReached()
    {
        owner.Character.Movement.StopMoving();

        if (entity)
        {
            var attack = new AttackState();
            attack.targetEntity = entity;
            owner.ChangeState(attack);
        }
        else
        {
            owner.ChangeState(new WaitState());
        }
    }

}
